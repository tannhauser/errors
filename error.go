package errors

import "fmt"
import "runtime/debug"

// ConstError wraps the built-in type string to implement the error interface.
type ConstError string

// Error implements the error interface for type ConstError.
func (e ConstError) Error() string {
	return string(e)
}

// Error wraps the following information and implement the error interface:
//
//	1. The source package the error occurs,
//	2. The operation cause the error,
//	3. The target resource,
//	4. The reason of the error, usually the error msg returned from the function call, and
//	5. The raw error object.
type Error struct {
	// src is for the package name.
	src string
	// op is the operation.
	op string
	// tgt is the target resource.
	tgt string
	// reason is the error msg returned from the function call.
	reason string
	// err is the raw error.
	err error
}

// New creates and returns an Error instance.
func New(op, tgt, reason string, err error) Error {
	return Error{
		src:    pkg(),
		op:     op,
		tgt:    tgt,
		reason: reason,
		err:    err,
	}
}

// Error implements the error interface for the type Error.
func (e Error) Error() string {
	if e.src == "" {
		e.src = pkg()
	}
	if e.reason != "" {
		return fmt.Sprintf("package: [%s], op: [%s], target: [%s], extra: [%s], error: [%v]", e.src, e.op, e.tgt, e.reason, e.err)
	}
	return fmt.Sprintf("package: [%s], op: [%s], target: [%s], error: [%v]", e.src, e.op, e.tgt, e.err)
}

// Raw returns the raw error from the function call.
func (e Error) Raw() error {
	return e.err
}

// pkg returns a string for caller package name.
func pkg() string {
	stack := string(debug.Stack())
	stackLen := len(stack)
	newlineCount := 0
	start := -1
	end := -1
	found := false
	for i := 0; i < stackLen; i++ {
		found = false
		if stack[i] == '\n' {
			newlineCount++
			found = true
		}
		if found && newlineCount == 7 {
			start = i
		}
		if found && newlineCount == 8 {
			end = i
			break
		}
	}
	if start == -1 || end == -1 {
		return ""
	}
	pkgNameHead := -1
	for i := start; i < end; i++ {
		if stack[i] == '/' {
			pkgNameHead = i
		}
	}
	if pkgNameHead == -1 {
		return ""
	}
	funcHead := -1
	funcTail := -1
	for i := pkgNameHead; i < end; i++ {
		if (stack[i] == '.' || stack[i] == '(') && funcHead != -1 {
			funcTail = i
			break
		}
		if stack[i] == '.' && funcHead == -1 {
			funcHead = i
		}
	}
	if funcHead == -1 || funcTail == -1 {
		return ""
	}
	return stack[start+1 : funcHead]
}
