package errors

import "fmt"
import "testing"

// ConstTestError tests the wrapping and unwrapping of error message for type Error.
func TestConstError(t *testing.T) {
	msg := "test"
	err := ConstError(msg)
	if err.Error() != msg {
		t.Errorf("failed to unwrap type Error, expected %s, got %v\n", msg, err)
	}
	return
}

// TestError tests the wrapping and unwrapping of error message for type Error
func TestError(t *testing.T) {
	msg := "test"
	op := "test"
	tgt := "event error"
	reason := ""
	packageName := pkg()
	raw := ConstError(msg)
	err := New(op, tgt, reason, raw)
	expected := fmt.Sprintf("package: [%s], op: [%s], target: [%s], error: [%v]", packageName, op, tgt, raw)
	if err.Error() != expected {
		t.Errorf("failed to unwrap type Error, expected [%s], got [%s]\n", expected, err.Error())
	}
	if err.Raw() != raw {
		t.Errorf("failed to get raw error, expected %v, got %v\n", raw, err.Raw())
	}
}
